import Ember from 'ember';

export function formatAddress([addresses, type]) {
  let primary= addresses.findBy('isPrimary');

  if(primary){
    return primary.value + " +(" + addresses.length-1 + ")";
  }
  return "";
}

export default Ember.Helper.helper(formatAddress);
