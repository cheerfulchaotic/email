import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { hasMany } from 'ember-data/relationships';

export default Model.extend({
  name: attr('string'),
  emailAddresses: hasMany ('address'),
  phoneNumbers: hasMany ('address'),
  primaryEmail: Ember.computed('emailAddresses.@each.isPrimary',function(){
    let primary = this.get('emailAddresses').findBy('isPrimary');
    if(primary){
      return primary.get('value');
    }
    return "";
  }),
  primaryPhone: Ember.computed('phoneNumbers.@each.isPrimary',function(){
    let primary = this.get('phoneNumbers').findBy('isPrimary');
    if(primary){
      return primary.get('value');
    }
    return "";
  }),
  moreEmails: Ember.computed('emailAddresses.length',function(){
    if(this.get('emailAddresses.length') > 1){
      return this.get('emailAddresses.length')-1;
    }
    return 0;
  }),
  morePhones: Ember.computed('phoneNumbers.length',function(){
    if(this.get('phoneNumbers.length') > 1){
      return this.get('phoneNumbers.length')-1;
    }
    return 0;
  })

});
